//
//  Models.swift
//  ComicBookStore
//
//  Created by Sam Ritchie on 13/11/2015.
//  Copyright © 2015 codesplice pty ltd. All rights reserved.
//

import UIKit

struct ComicBook {
    let productIdentifier: String
    let name: String
    let publisher: String
    let coverURL: NSURL
    var averageRating: Double?
    var numberRatings: Int = 0
}

// MARK: Equatable

extension ComicBook: Equatable {}

func ==(lhs: ComicBook, rhs: ComicBook) -> Bool {
    return lhs.productIdentifier == rhs.productIdentifier
        && lhs.name == rhs.name
        && lhs.publisher == rhs.publisher
        && lhs.coverURL == rhs.coverURL
        && lhs.averageRating == rhs.averageRating
        && lhs.numberRatings == rhs.numberRatings
}