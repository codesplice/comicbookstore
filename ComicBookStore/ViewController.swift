//
//  ViewController.swift
//  ComicBookStore
//
//  Created by Sam Ritchie on 13/11/2015.
//  Copyright © 2015 codesplice pty ltd. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    var comics = [ComicBook]()
    var prices = [SKProduct]()
    var images = [Int: UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let statusBarHeight = UIApplication.sharedApplication().statusBarFrame.height
        tableView.contentInset = UIEdgeInsetsMake(statusBarHeight, 0.0, 0.0, 0.0)
        loadComics()
    }

    func loadComics(from: Int = 1) {
        StoreAPI.sharedAPI.getComics(from: from).startWithNext { c in
            self.comics.appendContentsOf(c)
            self.tableView.reloadData()
            self.loadPrices(c.map { $0.productIdentifier })
        }
    }
    
    func loadPrices(ids: [String]) {
        StoreKit.sharedStoreKit.getProducts(ids).startWithNext { p in
            self.prices.appendContentsOf(p.products)
            self.tableView.reloadData()
        }
    }

    func loadThumbnail(indexPath: NSIndexPath, url: NSURL) {
        StoreAPI.sharedAPI.downloadCover(url).startWithNext { i in
            self.images[indexPath.row] = i
            self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
        }
    }

    // MARK: UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comics.count + 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard indexPath.row < comics.count else {
            let cell = tableView.dequeueReusableCellWithIdentifier("LoadingCell") as! LoadingCell
            cell.activityIndicator.startAnimating()
            return cell
        }

        let cell = tableView.dequeueReusableCellWithIdentifier("StoreItemCell", forIndexPath: indexPath) as! StoreItemCell
        let comic = comics[indexPath.row]
        
        cell.numberLabel.text = (indexPath.row + 1).description
        cell.titleLabel.text = comic.name
        cell.publisherLabel.text = comic.publisher
        if let rating = comic.averageRating {
            cell.ratingView.hidden = false
            cell.ratingView.rating = rating
            cell.ratingView.text = "(\(comic.numberRatings))"
        } else {
            cell.ratingView.hidden = true
        }
        
        if prices.count > indexPath.row {
            cell.downloadButton.hidden = false
            cell.downloadButton.state = .Buy
            cell.downloadButton.buyTitle = "$\(prices[indexPath.row].price.doubleValue)"
        } else {
            cell.downloadButton.hidden = true
        }
        
        if let image = images[indexPath.row] {
            cell.coverImageView.image = image
        } else {
            cell.coverImageView.image = UIImage(named: "placeholder")
            loadThumbnail(indexPath, url: comic.coverURL)
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row > 0 && indexPath.row == comics.count {
            loadComics(comics.count + 1)
        }
    }
}

class StoreItemCell: UITableViewCell {
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var coverImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var publisherLabel: UILabel!
    @IBOutlet var ratingView: CosmosView!
    @IBOutlet var downloadButton: PKDownloadButton!
}

class LoadingCell: UITableViewCell {
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
}