//
//  StoreKitWrapper.swift
//  ComicBookStore
//
//  Created by Sam Ritchie on 16/11/2015.
//  Copyright © 2015 codesplice pty ltd. All rights reserved.
//

import Foundation
import ReactiveCocoa

struct SKProduct {
    let localizedDescription: String
    let localizedTitle: String
    let price: NSDecimalNumber
    let priceLocale: String
    let productIdentifier: String
}

struct SKProductsResponse {
    let products: [SKProduct]
    let invalidProductIdentifiers: [String]
}

class StoreKit {
    static let sharedStoreKit = StoreKit()
    
    func getProducts(ids: [String]) -> SignalProducer<SKProductsResponse, NoError> {
        let products: [SKProduct] = ids.map { i in
            let randomPrice = Double(arc4random() % 5) + 0.99
            return SKProduct(localizedDescription: "", localizedTitle: "", price: NSDecimalNumber(double: randomPrice), priceLocale: "en-AU", productIdentifier: i)
        }
        let response = SKProductsResponse(products: products, invalidProductIdentifiers: [])
        
        return SignalProducer(value: response).delay(1, onScheduler:QueueScheduler.mainQueueScheduler)
    }
}