//
//  API.swift
//  ComicBookStore
//
//  Created by Sam Ritchie on 13/11/2015.
//  Copyright © 2015 codesplice pty ltd. All rights reserved.
//

import Foundation
import ReactiveCocoa

private let comics: [ComicBook] = {
    if let urls = NSBundle.mainBundle().URLsForResourcesWithExtension("jpg", subdirectory: "comics") {
        return urls.map { u in
            let randomRating: Double?
            let randomNumberRatings: Int
            let randomPrice = Double(arc4random() % 5) + 0.99
            if arc4random() % 3 == 0 {
                randomRating = Double(arc4random() % 90 + 10) / 20.0
                randomNumberRatings = Int(arc4random() % 50)
            } else {
                randomRating = nil
                randomNumberRatings = 0
            }
            return ComicBook(productIdentifier: NSUUID().UUIDString,
                name: u.URLByDeletingPathExtension!.lastPathComponent!,
                publisher: "DC Comics",
                coverURL: u,
                averageRating: randomRating,
                numberRatings: randomNumberRatings)
            
        }.shuffle()
    } else {
        return []
    }
}()

class StoreAPI {
    static let sharedAPI = StoreAPI()
    
    /// Simulates downloading lists of comic book records from the store
    func getComics(from from: Int = 1) -> SignalProducer<[ComicBook], NoError> {
        let list = Array(comics.dropFirst(from - 1).prefix(20))
        return SignalProducer(value: list).delay(1.0, onScheduler: QueueScheduler.mainQueueScheduler)
    }
    
    /// Simulates downloading a cover image from the network
    func downloadCover(coverURL: NSURL) -> SignalProducer<UIImage, NoError> {
        return SignalProducer(value: UIImage(contentsOfFile: coverURL.path!)!).delay(1.0, onScheduler: QueueScheduler.mainQueueScheduler)
    }
}